
//Déclaration du profil
const nom = 'Jutras, Jean-Philippe'
const dateNaissance = new Date(1982, 5, 25)
const sexe = 'M' // (H, h, M, m, F ou f)
const situationParticuliere = 'non'

//Combien de jours voulez-vous analyser?
let nbJour = prompt ('Sur combien de jours voulez-vous effectuer votre analyse?')
nbJour = parseInt(nbJour)


//Déclaration des variables nécessaire au calcul des consommations et à la validation du respect des recommandations.
let nbConsommationAlcool = 0
let journeesSansAlcool = 0
let JourneesExcedents = 0
let maxAlcoolSemaine = 0
let maxAlcoolJour = 0
let recommandationAlcoolRespectee = "VRAI"

//Création [tableau] + requête consommation/jour 
const tableau = []
let journeesAlcool = 0
let jourActif = ((nbJour-nbJour) + 1)
for (let i=0; i<nbJour; i++){
    journeesAlcool = prompt('Jour ' + (jourActif) + (': Combien d\'alcool avez-vous consommé?'))
    journeesAlcool = parseFloat(journeesAlcool)
    tableau.push(journeesAlcool)
    nbConsommationAlcool = (nbConsommationAlcool)+(journeesAlcool)
    nbConsommationAlcool = parseFloat(nbConsommationAlcool)
    jourActif = (jourActif + 1)
    if (journeesAlcool == 0){
        journeesSansAlcool = (journeesSansAlcool + 1)
    }
    if (journeesAlcool > maxAlcoolJour){
        JourneesExcedents = (JourneesExcedents + 1)
    }
}

//Déclaration maxAlcoolSemaine et maxAlcoolJour selon les recommandations
if (situationParticuliere === 'oui'){
    maxAlcoolSemaine = 0
    maxAlcoolJour = 0

} else if (sexe == 'H' || 'h' || 'M' || 'm') {
    maxAlcoolSemaine = 15
    maxAlcoolJour = 3

} else {
    maxAlcoolSemaine = 10
    maxAlcoolJour = 2
}

//Écrire Date et Nom
const maintenant = new Date()
console.log("Date: " + maintenant.toLocaleString())
console.log("Nom: " + nom)

//Calculer et écrire âge
const nbMillisecondes = maintenant - dateNaissance
let age = (nbMillisecondes / (1000 * 60 * 60 * 24 * 365))
age = Math.floor(age)
console.log("Âge: " + age + " an(s)")

//Écrire alcool
console.log("Alcool:")


//consommation par jour (Écrire moyenne)
let moyenneJour = (nbConsommationAlcool/nbJour)
moyenneJour = (moyenneJour).toFixed(2)
console.log("Moyenne par jour: " + (moyenneJour))

//consommation par semaine (Écrire sommeHebdo)
const moyenneSemaine = (moyenneJour * 7)
console.log("Consommation sur une semaine: " + (moyenneSemaine) + " Recommandation : " + (maxAlcoolSemaine))

//Maximum en une journée :
const max = Math.max(...tableau)
console.log("Maximum en une journée: " + (max) + " Recommandation : " + (maxAlcoolJour))

//ration des journées excédant
let rationJourneesExcedents = parseFloat(JourneesExcedents/nbJour)
rationJourneesExcedents = 100 * (rationJourneesExcedents).toFixed(2)
console.log("Ration de journées excédants: " + (rationJourneesExcedents) + "%")

//ration de journées sans alcool:
let rationJourneesSansAlcool = parseFloat(journeesSansAlcool/nbJour)
rationJourneesSansAlcool = 100 * (rationJourneesSansAlcool).toFixed(2)
console.log("Ration de journées sans alcool: " + (rationJourneesSansAlcool) + "%")

if ((moyenneSemaine > maxAlcoolSemaine) || (max > maxAlcoolJour)){
console.log("Vous ne respectez pas les recommandations.")
}else{
    console.log("Vous respectez les recommandations.")
}